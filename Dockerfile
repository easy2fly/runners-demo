FROM python:3.11.0-slim
WORKDIR /app
ADD . /app
RUN pip install --trusted-host pypi.python.org Flask
ENV NAME Summer
CMD ["python","app.py"]
